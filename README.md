# docker-elk

Make sure you're logged in to Docker

> ```docker login --username=frontinversionessrl```

Super secret password found in Confluence

> ```docker build elk --tag frontinversionessrl/elk```  
> ```docker push frontinversionessrl/elk```

From any instance simply login to this Docker account

> ```docker pull frontinversionessrl/elk```

To make the container:

> ```sudo docker run -p 5601:5601 -p 9200:9200 -p 5044:5044 -e "bootstrap.system_call_filter=false" --ulimit nofile=262144:262144 -it --name elk -d frontinversionessrl/elk```

To manage an existing container

> ```sudo docker start/stop elk```



Troubleshooting

Run the container without the -d option and read logs. Known to fail for lack of virtual memory. 
>	https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html 
